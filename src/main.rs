use std::env;

use serenity::{
    async_trait,
    model::{
        prelude::Message,
        gateway::Ready,
        guild::Member,
        guild::Role,
        id::GuildId,
        interactions::{
            application_command::{
                ApplicationCommandInteractionDataOptionValue,
                ApplicationCommandOptionType,
            },
            Interaction, InteractionResponseType,
        },
    },
    prelude::*,
    utils::MessageBuilder,
};

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            let content = match command.data.name.as_str() {
                "ping" => "Hey, I'm alive! @GreenPenguin".to_string(),
                "smartping" => {
                    let command_user = &command.user;
                    let message_text = match command
                        .data
                        .options
                        .iter()
                        .find(|option| option.name == "message")
                        .unwrap()
                        .resolved
                        .as_ref()
                        .unwrap()
                    {
                        ApplicationCommandInteractionDataOptionValue::String(text) => &text,
                        _ => "no text provided :(",
                    };
                    let guild_id = command.guild_id.expect("Expect guild ID");
                    let members: Vec<Member> = guild_id
                        .members(&ctx.http, None, None)
                        .await
                        .expect("Expect members");
                    let has_roles: Vec<Role> = command
                        .data
                        .options
                        .iter()
                        .filter(|option| option.name[..].starts_with("has-"))
                        .cloned()
                        .filter_map(|option| option.resolved)
                        .filter_map(|data| {
                            if let ApplicationCommandInteractionDataOptionValue::Role(role) = data {
                                Some(role)
                            } else {
                                None
                            }
                        })
                        .collect();
                    dbg!(&has_roles);
                    let not_roles: Vec<Role> = command
                        .data
                        .options
                        .iter()
                        .filter(|option| option.name[..].starts_with("not-"))
                        .cloned()
                        .filter_map(|option| option.resolved)
                        .filter_map(|data| {
                            if let ApplicationCommandInteractionDataOptionValue::Role(role) = data {
                                Some(role)
                            } else {
                                None
                            }
                        })
                        .collect();
                    dbg!(&not_roles);

                    let filtered_members: Vec<Member> = members
                        .iter()
                        .filter(|m| {
                            has_roles
                                .iter()
                                .all(|has_role| m.roles.contains(&has_role.id))
                                && (!not_roles
                                    .iter()
                                    .all(|not_role| m.roles.contains(&not_role.id))
                                    || not_roles.len() == 0)
                        })
                        .cloned()
                        .collect();

                    dbg!(&filtered_members);

                    let mut base_message = MessageBuilder::new()
                        .push("User ")
                        .push_bold_safe(&command_user)
                        .push(" mentioned the following people using `/smartping`: ")
                        .build();

                    let mention_message = filtered_members
                        .iter()
                        .fold(
                            &mut MessageBuilder::new(),
                            |message: &mut MessageBuilder, member| message.mention(member),
                        )
                        .build();

                    base_message.push_str(&mention_message);
                    base_message.push_str(" and sends you the following text: \"");
                    base_message.push_str(message_text);
                    base_message.push_str("\"");

                    base_message
                }
                "wonderful_command" => "Kacper says hi!".to_string(),
                _ => "not implemented :(".to_string(),
            };

            if let Err(why) = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content(content))
                })
                .await
            {
                println!("Cannot respond to slash command: {}", why);
            }
        }
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content.to_lowercase().contains("!ping") {
            // Sending a message can fail, due to a network error, an
            // authentication error, or lack of permissions to post in the
            // channel, so log to stdout when some error happens, with a
            // description of it.
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
                println!("Error sending message: {:?}", why);
            }
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);

        let guild_id = GuildId(
            env::var("GUILD_ID")
                .expect("Expected GUILD_ID in environment")
                .parse()
                .expect("GUILD_ID must be an integer"),
        );

        #[cfg(not(feature = "production"))]
        let guild_commands = GuildId::set_application_commands(&guild_id, &ctx.http, |commands| {
            commands.create_application_command(|command| {
                command
                    .name("smartping")
                    .description("Ping only the people with these roles")
                    .create_option(|option| {
                        option
                            .name("message")
                            .description("The message you want to send")
                            .kind(ApplicationCommandOptionType::String)
                            .required(true)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role1")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role2")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role3")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role1")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role2")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role3")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
            })
        })
        .await;

        #[cfg(feature = "production")]
        let global_command =
            ApplicationCommand::create_global_application_command(&ctx.http, |command| {
                command
                    .name("smartping")
                    .description("Ping only the people with these roles")
                    .create_option(|option| {
                        option
                            .name("message")
                            .description("The message you want to send")
                            .kind(ApplicationCommandOptionType::String)
                            .required(true)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role1")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role2")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("has-role3")
                            .description("Must have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role1")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role2")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("not-role3")
                            .description("Must not have this role")
                            .kind(ApplicationCommandOptionType::Role)
                            .required(false)
                    })
            })
            .await;

        #[cfg(not(feature = "production"))]
        println!(
            "I created the following guild slash commands: {:#?}",
            guild_commands
        );

        #[cfg(feature = "production")]
        println!(
            "I created the following global slash command: {:#?}",
            global_command
        );
    }
}

#[tokio::main]
async fn main() {
    // Configure the client with your Discord bot token in the environment.
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    // The Application Id is usually the Bot User Id.
    let application_id: u64 = env::var("APPLICATION_ID")
        .expect("Expected an application id in the environment")
        .parse()
        .expect("application id is not a valid id");

    // Build our client.
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .application_id(application_id)
        .await
        .expect("Error creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
